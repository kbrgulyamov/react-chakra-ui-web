import React from 'react';
import {
  ChakraProvider,
  theme,
} from '@chakra-ui/react';
import {  Header } from './Components/Header/Header'
import { MainPa } from './Components/Main/MinPage'
import { BlockInvest } from './Components/InvestingBlock/investing'
import { CashBlock } from './Components/CashBlock/Chash'
import { SavingBlock } from './Components/BlueBlock/Saving'

function App() {
  return (
    <ChakraProvider theme={theme} >
      <Header/>
      <MainPa />
      <BlockInvest />
      <CashBlock />
      <SavingBlock/>
    </ChakraProvider>
  );
}

export default App;
