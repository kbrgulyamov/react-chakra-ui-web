import { Box, Button, Container, Flex, Heading, Image, Text } from "@chakra-ui/react";
import React from "react";
import { AspectRatio } from '@chakra-ui/react'
import ImgMain from '../Main/img/Thousand-03.svg'


const MainPa = () => {
    return (
      <Container maxW='1300px'>
        <Box pt={130} display='flex' gap={59}>
          <Image w={630} src={ImgMain} alt='naruto' objectFit='cover' />
          <Box display='flex' flexDirection='column' pl={50} mt='126px' gap='34px'>
            <Heading as='h1' fontSize='53px' fontWeight={400}>Make wealth<br /> your own.</Heading>
            <Box>
              <Text>Personalized, Automated, Effortless<br/> Investing and Savings.</Text>
            </Box>
            <Button colorScheme='telegram'>Telegram</Button>
          </Box>
        </Box>
        
        <Box display='flex' gap='125px' mt={13}>
          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>450K+</Heading>
            <Text textAlign='center' fontWeight={500}>Trusted clients</Text>
          </Box>

          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>$26B+</Heading>
            <Text textAlign='center' fontWeight={500}>In client funds</Text>
          </Box>

          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>$976B+</Heading>
            <Text textAlign='center' fontWeight={500}>In client funds</Text>
          </Box>
          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>$76K+</Heading>
            <Text textAlign='center' fontWeight={500}>In client funds</Text>
          </Box>
          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>$8K+</Heading>
            <Text textAlign='center' fontWeight={500}>In client funds</Text>
          </Box>

          <Box display='flex' flexDirection='column' >
            <Heading as='h1' fontWeight={400} textAlign='center'>$90K+</Heading>
            <Text textAlign='center' fontWeight={500}>In client funds</Text>
          </Box>
        </Box>
      </Container>
    )
}

export { MainPa }