import React from "react";
import { Box, Container, Flex, Heading, Text, Button } from '@chakra-ui/react';
import VisaImg from '../CashBlock/img/visa.svg'

const CashBlock = () => {
       return (
              <Container maxW='1200px' position='relative' right='15px'>
                     <Flex gap={5} id='cash'>
                            <Box position='relative' top='-30px'>
                                   <img width='700px' src={VisaImg} alt="" />
                            </Box>
                            <Box display='flex' flexDirection='column' gap={7}>
                            <Heading as='h1' fontWeight={400} fontSize='56px' lineHeight='64px'>Everyday cash,<br /> isn’t easy.<br /> We just<br /> make it feel<br /> that way.</Heading>
                                   <Text w={352} fontWeight={500}>Let us customize a portfolio just for you or build it yourself, and our powerful automation will make the most of your investments, optimizing your performance and helping lower your taxes.</Text>
                                   <Box display='flex' gap={4}>
                                          <Button colorScheme='telegram'  w={144} borderRadius={3}>Start investing</Button>
                                          <Button borderRadius={3} w={36}>Call us</Button>
                            </Box>
                            </Box>
                     </Flex>
              </Container>
       )
}

export { CashBlock }