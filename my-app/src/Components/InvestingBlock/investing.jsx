import React from "react";
import { Container, Box, Heading, Text, Flex, Button } from '@chakra-ui/react';
import inveStImg from '../InvestingBlock/img/invest.svg'


const BlockInvest = () => {
       return (
              <Container maxW='1100px' mt='99px' >
                     <Flex gap={30} id="Invest">
                     <Box display='flex' flexDirection='column' gap={7}>
                            <Heading as='h1' fontWeight={400} fontSize='56px' lineHeight='64px'>Investing<br /> isn’t easy.<br /> We just<br /> make it feel<br /> that way.</Heading>
                                   <Text w={352} fontWeight={500}>Let us customize a portfolio just for you or build it yourself, and our powerful automation will make the most of your investments, optimizing your performance and helping lower your taxes.</Text>
                                   <Box display='flex' gap={4}>
                                          <Button colorScheme='telegram'  w={144} borderRadius={3}>Start investing</Button>
                                          <Button borderRadius={3} w={36}>Call us</Button>
                            </Box>
                            </Box>
                            <Box position='relative' top='-125px' left='60px'>
                                   <img  src={inveStImg} alt="" />
                            </Box>
                     </Flex>
              </Container>
       )
}

export { BlockInvest }