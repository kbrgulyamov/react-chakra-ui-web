import React from "react";
import { Box, Container, Flex, Heading, Text } from '@chakra-ui/react';
import ImgBlueBlock from '../BlueBlock/img/shit.svg'

const SavingBlock = () => {
       return (
              <Box bg='#4840BB' h='500px' w='100%'>
                     <Container maxW='1100px'>
                     <Flex gap={40}>
                     <Box display='flex' flexDirection='column' gap={3} pt='80px'>
                     <Heading as='h1' fontWeight={400} fontSize='53px' color='#fff' lineHeight='64px'>Saving and<br /> investing.<br /> We just<br /> that way.</Heading>
                     <Text w={352} color='#fff' fontWeight={500}>Let us customize a portfolio just for you or build it yourself, and our powerful automation will make the most of your investments, optimizing your performance and helping lower your taxes.</Text>
                     </Box>
                     <Box position='relative' top='65px'>
                     <img width='500px' src={ImgBlueBlock} alt="" />
                     </Box>
                     </Flex>       
                     </Container>
              </Box>
       )
}

export { SavingBlock }