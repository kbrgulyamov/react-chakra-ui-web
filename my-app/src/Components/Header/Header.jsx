import React from "react";
import { Image, Flex, Container, Box, Link, useColorMode, Button, Heading } from '@chakra-ui/react';
import { ColorModeSwitcher } from './ColorModeSwitcher';
import Logo from '../Header/img/Frame.svg'


const Header= (props) => {
      const { colorMode } = useColorMode();
      

      return (
            <Box as="header" w='100%' h={59} pos='fixed' bg={colorMode === 'dark' ? 'gray.600' : 'gray.200'} zIndex={999}>

                  <Container maxW='1300px' p='11px' >
                        <Flex alignItems={"center"} justifyContent="space-between">
                              <Heading fontSize='30px' as='h1'>Wealthfront</Heading>
                              <Box display='flex' justifyContent='center' alignItems='center' gap={10} p='9px'>
                              <Link  href="#Invest">Invest</Link>
                              <Link href="#cash">Cash</Link>
                              <Link href="/Dragon">Borrow</Link>
                              <Link href="/Updates">Updates</Link>
                              </Box>
                              <Box display='flex' gap={3}>
                                    <ColorModeSwitcher />
                                    <Button colorScheme='#4840BB' variant='outline' >Login In</Button>
                                    <Button colorScheme='telegram'>Get started</Button>
                              </Box>
                        </Flex>
                  </Container>
      
            </Box>
      )
}

export  {Header}
